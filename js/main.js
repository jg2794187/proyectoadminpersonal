  // Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } from
"https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyA9vxEOeURhyjqAUxe80Q23xmA3qQvz4Lc",
  authDomain: "adminperfumeria.firebaseapp.com",
  databaseURL: "https://adminperfumeria-default-rtdb.firebaseio.com",
  projectId: "adminperfumeria",
  storageBucket: "adminperfumeria.appspot.com",
  messagingSenderId: "1033097769946",
  appId: "1:1033097769946:web:22f3f2699b851a078c46e3"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

//variables para el manejo de la imagen

const imgeInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progressDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');

//Declara unas variables global
var numSerie = 0;
var nombre= "";
var marca = "";
var precio = "";
var urlImag = "";

//Funciones
function leerInputs() {
     numSerie = document.getElementById('txtNumSerie').value;
     nombre = document.getElementById('txtNombre').value;
     marca = document.getElementById('txtMarca').value;
     precio = document.getElementById('txtPrecio').value;
     urlImag = document.getElementById('txtUrl').value;
}

function mostrarMensaje(mensaje) {
    var mensajeElement = document.getElementById("mensaje");
    mensajeElement.textContent = mensaje;
    mensajeElement.style.display = "block";
    setTimeout(() => { mensajeElement.style.display = "none" }, 1000);
}

//Agregar productos a la DB
const btnAgregar = document.getElementById('btnAgregar');
btnAgregar.addEventListener('click', insertarProducto);

function insertarProducto() {
    alert("Ingrese a add db");
    leerInputs();
    //validar
    if (numSerie==="" || nombre==="" || marca==="" || precio==="") {
        mostrarMensaje("Faltaron Datos por Capturar");
        return;
    }
    // --- Funcion de FireBase para agregar registro
    set(
        refS(db, "Perfumeria/" + numSerie),
        {
            /* Datos a guardar 
          realizar json con los campos y datos de la tabla 
          campo: valor
        */
            numSerie:numSerie,
            nombre:nombre,
            marca:marca,
            precio:precio,
            urlImag:urlImag
        }
    ).then(() => {
        alert("Se agrego con exito");
    }).catch((error) => {
        alert("Ocurrio un error");
    });
    Listarproductos();
}

function limpiarInputs(){
    document.getElementById('txtNumSerie').value ='';
    document.getElementById('txtNombre').value ='';
    document.getElementById('txtMarca').value ='';
    document.getElementById('txtPrecio').value ='';
    document.getElementById('txtUrl').value ='';

  }

  function escribirInputs(){
    document.getElementById('txtNombre').value =nombre;
    document.getElementById('txtMarca').value =marca;
    document.getElementById('txtPrecio').value =precio;
    document.getElementById('txtUrl').value =urlImag;
  }

function buscarProducto() {
    const numSerie = document.getElementById('txtNumSerie').value.trim();
    if (numSerie === "") {
        mostrarMensaje("No se ingreeso un Num de Serie");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'Perfumeria/' + numSerie)).then((snapshot) => {
        if (snapshot.exists()) {
            nombre = snapshot.val().nombre;
            marca = snapshot.val().marca;
            precio = snapshot.val().precio;
            urlImag = snapshot.val().urlImag;
            escribirInputs();
        } else {
            limpiarInputs();
            mostrarMensaje("El producto con codigo " + numSerie + "No Existe:(");
        }
    })
}

const btnBuscar = document.getElementById('btnBuscar');
btnBuscar.addEventListener('click', buscarProducto);

//Listar Productos

function Listarproductos() {
    const dbref = refS(db, 'Perfumeria');
    const tabla = document.getElementById('tablaProductos');
    const tbody = tabla.querySelector('tbody');
    tbody.innerHTML = '';
    onValue(dbref, (snapshot) => {
        snapshot.forEach(childSnapshot => {
            const childKey = childSnapshot.key;
            const data = childSnapshot.val();
            var fila = document.createElement('tr');

            var celdaCodigo = document.createElement('td');
            celdaCodigo.textContent = childKey;
            fila.appendChild(celdaCodigo);

            var celdaNombre = document.createElement('td');
            celdaNombre.textContent = data.nombre;
            fila.appendChild(celdaNombre);

            var celdaPrecio = document.createElement('td');
            celdaPrecio.textContent = data.marca;
            fila.appendChild(celdaPrecio);

            var celdaCantidad = document.createElement('td');
            celdaCantidad.textContent = data.precio;
            fila.appendChild(celdaCantidad);

            var celdaImagen = document.createElement('td');
            var imagen = document.createElement('img');
            imagen.src = data.urlImag;
            imagen.width = 100;
            celdaImagen.appendChild(imagen);
            fila.appendChild(celdaImagen);
            tbody.appendChild(fila);
        });
    }, { onlyOnce: true });
}

Listarproductos();

//Funcion actualizar

function actualizarProducto() {
    leerInputs();
    if (numSerie === "" || nombre === "" || marca === "" || precio === "") {
        mostrarMensaje("Favor de capturar toda la informacion");
        return;
    }
    alert("Actualizar");
    update(refS(db, 'Perfumeria/' + numSerie), {
        numSerie: numSerie,
        nombre: nombre,
        marca: marca,
        precio: precio,
        urlImag: urlImag
    }).then(() => {
        mostrarMensaje("Se actualizo con exito");
        limpiarInputs();
        Listarproductos();
    }).catch((error) => {
        mostrarMensaje("Ocurrio un error: " + error);
    });
}

const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarProducto);

//Funcion Borrar

function eliminarProducto() {
    const numSerie = document.getElementById('txtNumSerie').value.trim();
    if (numSerie === "") {
        mostrarMensaje("No se ingreso un Codigo Valido.");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'Perfumeria/' + numSerie)).then((snapshot) => {
        if (snapshot.exists()) {
            remove(refS(db, 'Perfumeria/' + numSerie)).then(() => {
                mostrarMensaje("Perfume eliminado con éxito.");
                limpiarInputs();
                Listarproductos();
            }).catch((error) => {
                mostrarMensaje("Ocurrio un error al eliminar el perfume: " + error);
            });
        } else {
            limpiarInputs();
            mostrarMensaje("El perfume con ID " + numSerie + " no existe.");
        }
    });
}

const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click', eliminarProducto);

uploadButton.addEventListener('click', (event) =>{
    event.preventDefault();
    const file = imageInput.files[0];

    if(file){
      const storageRef = ref(storage, file.name);
      const uploadTask = uploadBytesResumable(storageRef, file);
      uploadTask.on('state_changed', (snapshot)=>{
        const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
       progressDiv.textContent = 'Progreso:' + progress.toFixed(2) + '%';
      },(error)=>{
        console.error(error);
      }, ()=>{
        getDownloadURL(uploadTask.snapshot.ref).then((downloadURL)=>{
          txtUrlInput.value = downloadURL;
          setTimeout(()=>{
            progressDiv.textContent = '';
          }, 500);
        }).catch((error)=>{
          console.error(error);
        });
      });
    }
  });